
export interface ItemProps {
    id: string
    price: string
    img: string
    info: string
    rate: string
    des: string
    fav?: boolean
    hidden?: boolean
}
export interface SliderValue {
  min: number
  max: number
}
export interface WatchList { 
  [id: string]: ItemProps
}