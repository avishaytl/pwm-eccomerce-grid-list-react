import { GridSwitchChild, GridViewSwitch, TopFilterBarChild } from "../style"
import {IoGridSharp} from 'react-icons/io5'
import {FaThList} from 'react-icons/fa' 
import {IoIosAddCircle} from 'react-icons/io' 
import { useCallback } from "react"
import { useStore } from "../../../stores"
import { Observer } from "mobx-react-lite"

const GridSwitch = () => {
    const {mainStore} = useStore() 
    const setList = useCallback(()=>{
        mainStore?.setGridViewMode('list')
    },[])
    const setGrid = useCallback(()=>{
        mainStore?.setGridViewMode('grid')
    },[])
    // const setNew = useCallback(()=>{ 
    //     mainStore?.setWatchListMode()
    // },[])
    return(
        <TopFilterBarChild flexEnd>
            <Observer>
                {()=> <GridViewSwitch>
                        {/* <GridSwitchChild onClick={setNew} selected={mainStore?.WatchListMode ? true : false} fontSize={24}>
                            <IoIosAddCircle/> 
                        </GridSwitchChild> */}
                        <GridSwitchChild onClick={setList} selected={mainStore?.gridViewMode === 'list'} fontSize={24}>
                            <FaThList/>
                        </GridSwitchChild>
                        <GridSwitchChild onClick={setGrid} selected={mainStore?.gridViewMode === 'grid'} fontSize={24}>
                            <IoGridSharp/> 
                        </GridSwitchChild>
                    </GridViewSwitch>}
            </Observer>
        </TopFilterBarChild>
    )
}

export default GridSwitch