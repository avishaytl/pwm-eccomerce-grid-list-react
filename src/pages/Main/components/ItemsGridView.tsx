import { Observer } from "mobx-react-lite"
import { useCallback, useEffect, useState } from "react"
import { ItemProps } from "../../../interfaces"
import { useStore } from "../../../stores"
import { ItemsGridViewConatiner } from "../style"
import ItemGrid from "./ItemGrid"

const ItemsGridView = () => {
    const {mainStore} = useStore()
    const [isReady,setReady] = useState(false)
    useEffect(()=>{
        (async()=>{
            await mainStore?.fetchGridData()
            setReady(true)
        })()
    },[])
    const onItemClick = useCallback((remove: boolean, index:number, item: ItemProps)=>{
        mainStore?.setGridWatchClick(remove, index, item)
    },[])
    return( 
        <Observer>
            {()=> <>
                {/* {mainStore?.WatchListMode && 'Add New Item!'} */}
                {(mainStore?.items ? mainStore?.items : []).length === 0 && 'Cant Find Items!'}
                {isReady && <ItemsGridViewConatiner display={mainStore?.gridViewMode === 'list' ? 'flex' : 'grid'}>  
                    {(mainStore?.items ? mainStore?.items : []).map(({hidden, fav, id, des, price, img, info, rate },index: number)=>{ 
                        let strPrice = price.split('.')[0] ? price.split('.')[0] : price
                        let pri = typeof parseInt(strPrice.substring(1,strPrice.length)) === 'number' ? parseInt(strPrice.substring(1,strPrice.length)) : null
                        if(typeof pri === 'number' && mainStore?.sliderValue && (mainStore?.sliderValue.min > pri || mainStore?.sliderValue.max < pri))
                            return null
                        return(
                            <ItemGrid
                                watchListKeys={(mainStore?.watchList ? Object.keys(mainStore?.watchList) : [])}
                                key={index} 
                                {...{hidden, index, fav, onItemClick, id, des, price, img, info, rate }}/>
                        )
                    })}
                </ItemsGridViewConatiner>}
            </> }
        </Observer>
    )
}

export default ItemsGridView