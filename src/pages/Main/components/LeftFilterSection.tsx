import { LeftFilterCustReviewGridChild, LeftFilterIcon, LeftFilterSectionChild, LeftFilterSectionConatiner, LeftFilterText } from "../style"
import {IoIosArrowUp} from 'react-icons/io'
import SliderValue from "./SliderValue"
import CustReviewBar from "./CustReviewBar"
import { useStore } from "../../../stores"
import { observer } from "mobx-react-lite"

const LeftFilterSection = () => {
    const {mainStore} = useStore() 
    const ObsFilter = observer(()=>{
        return(
            <LeftFilterSectionConatiner className={window.innerWidth >= 750 ? `web-open` : mainStore?.showModileFilter ? `mobile-open` : `mobile-close`}>
                <LeftFilterSectionChild height={50}>
                    <LeftFilterText bold>
                        Price Range Selected
                    </LeftFilterText>
                </LeftFilterSectionChild>
                <LeftFilterSectionChild height={100}>
                    <SliderValue/>
                </LeftFilterSectionChild>
                <LeftFilterSectionChild height={150}>
                    <LeftFilterCustReviewGridChild center>
                        <LeftFilterText bold>
                            CUSTOMER REVIEWS
                        </LeftFilterText>
                        <LeftFilterIcon>
                            <IoIosArrowUp/>
                        </LeftFilterIcon>
                    </LeftFilterCustReviewGridChild>
                    <CustReviewBar/>
                </LeftFilterSectionChild>
            </LeftFilterSectionConatiner>
        ) 
    })
    return <ObsFilter/>
}

export default LeftFilterSection