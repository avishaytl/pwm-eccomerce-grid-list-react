import { Observer } from "mobx-react" 
import { useStore } from "../../../stores"
import { ItemsTopGridBarChild, ItemsTopGridBarConatiner, ItemsTopGridBarText, ItemsTopGridBarView } from "../style"

const ItemsTopGridBar = () => { 
    const {mainStore} = useStore() 
    return(
        <ItemsTopGridBarConatiner> 
            <Observer>
                {()=><ItemsTopGridBarView>
                    {mainStore?.lastUserSearchHistory.map((str, index)=>{
                        const onClick = () =>{
                            mainStore?.setCurrentUserSearchValue(str)
                            mainStore?.setGridData(!index)
                        }
                        return(
                            <ItemsTopGridBarChild onClick={onClick} key={index}>
                                <ItemsTopGridBarText selected={mainStore?.currentUserSearchValue === str}>
                                    {str}
                                </ItemsTopGridBarText>
                            </ItemsTopGridBarChild>)
                    })}
                </ItemsTopGridBarView>}
            </Observer>
        </ItemsTopGridBarConatiner>
    )
}

export default ItemsTopGridBar