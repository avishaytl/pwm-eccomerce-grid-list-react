import React from "react" 
import { GridView, MainContainer, MainView } from "../style"
import Header from "./Header"
import ItemsGridView from "./ItemsGridView"
import ItemsTopGridBar from "./ItemsTopGridBar"
import LeftFilterSection from "./LeftFilterSection"
import TopFilterBar from "./TopFilterBar"

const Main = () => { 
    return(
        <MainContainer>
            <Header/>
            <MainView>
                <LeftFilterSection/>
                <GridView>
                    <TopFilterBar/>
                    <ItemsTopGridBar/>
                    <ItemsGridView/>
                </GridView>
            </MainView>
        </MainContainer>
    )
}

export default React.memo(Main)