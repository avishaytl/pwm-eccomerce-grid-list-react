import Box from '@mui/material/Box'
import Slider from '@mui/material/Slider'
import Tooltip from '@mui/material/Tooltip' 
import { Observer } from 'mobx-react-lite'
import { useCallback, useState } from 'react'
import { useStore } from '../../../stores'
import debounce from 'lodash.debounce'
function valuetext(value) {
  return `${value}°C`;
}

function ValueLabelComponent(props) {
  const { children, value } = props;

  return (
    <Tooltip open enterTouchDelay={0} placement="top" title={value}>
      {children}
    </Tooltip>
  );
}

function SliderValue() {
  const {mainStore} = useStore() 
  const [value, setValue] = useState([mainStore?.sliderValue.min as number, mainStore?.sliderValue.max as number]);

  const setSliderValue = useCallback((val) => { 
    const [min, max] = val 
    mainStore?.setSliderValue({min, max}) 
  },[]);

  const handleChange = useCallback((event, newValue) => {
    setValue(newValue); 
    handleDebounce(newValue)
  },[]);

  const handleDebounce = useCallback(debounce(setSliderValue, 350),[])

  return (
      <Box sx={{ width: 200,paddingTop:2 }}>
         <Observer>
           {()=> <Slider
                    disabled={mainStore?.watchListMode}
                    getAriaLabel={(v)=> v+'$'}
                    valueLabelFormat={(v)=> v+'$'}
                    value={value}
                    components={{
                      ValueLabel: ValueLabelComponent,
                    }}
                    max={200}
                    onChange={handleChange}
                    valueLabelDisplay="on"
                    getAriaValueText={valuetext}
                  />}
         </Observer>
      </Box>
  );
}

export default SliderValue
