import { useStore } from "../../../stores"
import { DropDownFilterBar, TopFilterBarChild, TopFilterBarConatiner, TopFilterBarView } from "../style"
import DropDownFilterBarChild from "./DropDownFilterBarChild"
import GridSwitch from "./GridSwitch"

const TopFilterBar = () => {
    const {mainStore} = useStore()
    const sortby = [{label:'Useless first',value: 1},{label:'Cheapest',value: 2},{label:'Most Expensive',value: 3},{label:'Top Rate',value: 4}]
    const options = [{label:'Opt 1',value: 1},{label:'Opt 2',value: 2},{label:'Opt 3',value: 3}] 
    const onChangeValue = (val: string) => {
        mainStore?.setFilter(val)
    }
    return(
        <TopFilterBarConatiner>
            <TopFilterBarView>
                <TopFilterBarChild>
                    <DropDownFilterBar>
                        <DropDownFilterBarChild top placeholder={'Useless first'} options={sortby} {...{onChangeValue}}/>
                    </DropDownFilterBar>
                    <DropDownFilterBar>
                        <DropDownFilterBarChild placeholder={'Condition'} {...{onChangeValue, options}}/>
                    </DropDownFilterBar>
                    <DropDownFilterBar>
                        <DropDownFilterBarChild placeholder={'Delivery options'} {...{onChangeValue, options}}/>
                    </DropDownFilterBar>
                </TopFilterBarChild>
                <GridSwitch/>
            </TopFilterBarView>
        </TopFilterBarConatiner>
    )
}

export default TopFilterBar