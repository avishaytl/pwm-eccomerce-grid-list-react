import { Observer } from "mobx-react-lite"
import { useStore } from "../../../stores"
import { HeaderChild, HeaderConatiner, LogoIcon, LogoText, UserFilterView, UserImg, UserImgCircle, UserImgView, WatchBtn, WatchBtnCircle, WatchBtnText} from "../style"
import HeaderSearchBar from "./HeaderSearchBar"
import { FcFilledFilter } from 'react-icons/fc'


const Header = () => {
    const {mainStore} = useStore() 
    return(
        <HeaderConatiner>
            <HeaderChild logo>
                <LogoIcon>S</LogoIcon>
                <LogoText>Shopka</LogoText>
            </HeaderChild>
            <HeaderChild search>
                <HeaderSearchBar/>
            </HeaderChild>
            <HeaderChild user>
                <Observer>
                    {()=> <WatchBtn onClick={mainStore?.setUserWatchList}> 
                                <WatchBtnCircle>{Object.keys(mainStore?.watchList ? mainStore?.watchList : {}).length}</WatchBtnCircle> 
                                <WatchBtnText open={mainStore?.watchListMode} bold>Watch</WatchBtnText>
                            </WatchBtn>}
                </Observer> 
                <UserImgView>
                    <UserFilterView onClick={mainStore?.actionMobileFilter}>
                        <FcFilledFilter/>
                    </UserFilterView>
                    <UserImgCircle>
                        <UserImg src="https://www.pavilionweb.com/wp-content/uploads/2017/03/man-300x300.png"/> 
                    </UserImgCircle>
                </UserImgView> 
            </HeaderChild>
        </HeaderConatiner>
    )
}

export default Header