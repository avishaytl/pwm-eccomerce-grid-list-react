import { useCallback } from "react"
import { useStore } from "../../../stores"
import { CustReviewChild, LeftFilterCustReviewGrid, LeftFilterCustReviewGridChild, LeftFilterText } from "../style"


const CustReviewBar = () => {
    const {mainStore} = useStore()
    const stars = [`⭐⭐⭐⭐`,`⭐⭐⭐`,`⭐⭐`,`⭐`] 
    let childs: JSX.Element[] = []
    let rows: JSX.Element[] = []
    stars.map((str, index)=>{
        const onClick = () =>{
            actionSearch(str.length)
        }
        if(index < 2){
            childs.push(<CustReviewChild key={index + str} onClick={onClick}>
                        {str}
                        <LeftFilterText>
                            {`& up `}
                        </LeftFilterText>
                    </CustReviewChild>)
        }else{
            if(index === 2){
                rows.push( 
                    <LeftFilterCustReviewGridChild key={index + str}>
                        {childs}
                    </LeftFilterCustReviewGridChild>
                )
                childs = []
            }
            childs.push(<CustReviewChild key={index + str} onClick={onClick}>
                        {str}
                        <LeftFilterText>
                            {`& up `}
                        </LeftFilterText>
                    </CustReviewChild>)
        } 
    }) 
    const actionSearch = useCallback((val:number)=>{
        if(!mainStore?.watchListMode)
            mainStore?.setGridData(false,`${val}`, true)
    },[])
    
    return( 
        <LeftFilterCustReviewGrid> 
            {rows}
            <LeftFilterCustReviewGridChild> 
                {childs}
            </LeftFilterCustReviewGridChild> 
        </LeftFilterCustReviewGrid>
    )
}

export default CustReviewBar