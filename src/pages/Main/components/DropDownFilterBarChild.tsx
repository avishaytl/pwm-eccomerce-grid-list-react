import { Observer } from "mobx-react-lite"
import { useCallback } from "react"
import { useStore } from "../../../stores"
import { DropDownFilterBarChildSelect, DropDownFilterBarChildView, DropDownFilterTopLabel } from "../style"

interface DropDownOptProps {
    label: string
    value: number
}

const DropDownFilterBarChild = ({options, placeholder, top, onChangeValue}:{options: DropDownOptProps[], placeholder?: string, top?: boolean, onChangeValue: (val: string)=>void}) => { 
    const {mainStore} = useStore()
    const onChange = useCallback((e)=>{
        const index:number = e.target.value 
        if(options[index - 1] && top)
            onChangeValue(options[index - 1].label)
    },[])
    return(
        <DropDownFilterBarChildView>
            {top && <DropDownFilterTopLabel>SORT BY</DropDownFilterTopLabel>} 
            <Observer>
                {()=><DropDownFilterBarChildSelect disabled={mainStore?.watchListMode} defaultValue={''} onChange={onChange} placeholder={placeholder}>
                        <option value="" disabled hidden>{placeholder}</option>
                        {options.map((opt)=>{
                            return(
                                <option key={opt.label} value={opt.value}>{opt.label}</option> 
                            )
                        })} 
                    </DropDownFilterBarChildSelect>}
            </Observer>
        </DropDownFilterBarChildView>
    )
}

export default DropDownFilterBarChild