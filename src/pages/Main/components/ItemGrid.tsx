import { AdditionalInfo, DescText, ImgView, ItemBottomBar, ItemGridView, ItemGridViewRow, ItemPrice, RateText, WatchBtnText, WatchItemBtn } from "../style"
import {BsHeart} from 'react-icons/bs'
import { ItemProps } from "../../../interfaces"
import { Observer } from "mobx-react-lite"

const ItemGrid = ({hidden, index, watchListKeys, fav, onItemClick, id, des, price, img, info, rate}:{hidden?: boolean, index:number, watchListKeys: string[], fav?: boolean,onItemClick: (remove: boolean, index:number, item: ItemProps)=>void, id:string, des: string, price: string, img: string, info: string, rate: string}) => {
    const onClick = () => {
        onItemClick(!fav, index, {fav, id, des, price, img, info, rate})
    }
    return(
        <ItemGridView key={id}>
            <ItemGridViewRow>
                <ImgView src={img}/>
                <ItemGridView flexView>
                    <DescText>{des}</DescText>
                    <ItemPrice>{price}</ItemPrice>
                    <AdditionalInfo>{info}</AdditionalInfo>  
                </ItemGridView>
            </ItemGridViewRow>
            {!hidden && <ItemBottomBar onClick={onClick}>
                <RateText>{rate}&nbsp;{rate.length}</RateText>
                <WatchItemBtn>
                    <WatchBtnText fontSize={14}>
                        <Observer>
                            {()=><BsHeart color={watchListKeys.includes(id) ? 'red' : '#2264D1'} fontSize={12}/>}
                        </Observer>
                        &nbsp;Watch
                    </WatchBtnText>
                </WatchItemBtn>
            </ItemBottomBar>}
        </ItemGridView>
    )
}

export default ItemGrid