import { SearchBar, SearchIcon, SearchInput} from "../style"
import { FiSearch } from 'react-icons/fi'
import { GrFormClose } from 'react-icons/gr'
import { useCallback, useState } from "react"
import { useStore } from "../../../stores" 
import { Observer } from "mobx-react-lite"

const HeaderSearchBar = () => {
    const {mainStore} = useStore()
    const [value,setValue] = useState('')   

    const onSearch = (e,str?: string)=>{ 
        mainStore?.setUserSearch(str ? str : value)
    }

    const onChange = useCallback((e)=>{ 
        setValue(e.target.value)
    },[])

    const onKeyDown = useCallback((e)=>{
        if (e.key === 'Enter') {
            e.target.blur(); 
            onSearch(null,e.target.value)
        }
    },[])

    const clearSearch = useCallback(()=>{
        setValue('')
    },[])
 

    return( 
        <SearchBar>
            <SearchIcon onClick={onSearch}>
                <FiSearch/>
            </SearchIcon>
            <Observer>
                {()=><SearchInput disabled={mainStore?.watchListMode} onChange={onChange} value={value} onKeyDown={onKeyDown}/>}
            </Observer>
            <SearchIcon onClick={clearSearch}>
                <GrFormClose/>
            </SearchIcon>
        </SearchBar>
    )
}

export default HeaderSearchBar