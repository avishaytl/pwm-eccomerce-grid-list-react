import styled from 'styled-components'

export const MainContainer = styled.main`
    max-width: 100vw;
    width: 1300px;
    @media (max-width: 1300px) {
        padding: 0px 20px 0px 20px;
    }
    @media (max-width: 750px) {
        padding: 0px;
    }
`
export const MainView = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    min-width: 100%;
    max-width: 100%;
    flex: 1;
    transition: all 0.5s ease;
    @media (max-width: 1000px) {
        flex-direction: column; 
        min-width: 100%;
    }
    @media (max-width: 750px) {
        max-width: 100vw;
        align-items: center;
        justify-content: flex-start;
    }
`
export const GridView = styled.div` 
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    transition: all 0.5s ease;
    @media (max-width: 1000px) {
        min-width: 100%;
    }
`
export const HeaderConatiner = styled.header`
    min-width: 100%;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    min-height: 70px;
    display: flex;
    flex-direction: row;
    z-index: 12;
    ovreflow: hidden;
    @media (max-width: 750px) {
        flex-direction: column;
        background: #fff;
        min-height: 160px;
        max-height: 160px;
    }
`
export const HeaderChild = styled.div<{search?: boolean, logo?: boolean, user?: boolean}>`
    min-width: ${p => p.logo ? '250px' : p.user ? '150px' : 'calc(100% - 250px - 150px)'};
    min-height: 100%;
    display: flex;
    align-items: center;
    background: #fff;
    justify-content: ${p => p.logo ? 'center' :'space-between'};
    @media (max-width: 750px) {
        justify-content: ${p => !p.user ? 'center' :'space-between'};
        padding: ${p => !p.user ? '0px' :'0px 10px 0px 10px'};
        min-height: ${p => p.logo ? '60px' :'auto'};
    }
`
export const LogoIcon = styled.div`
    user-select: none;  
    pointer: default; 
    display: flex;
    align-items: center;
    justify-content: center;
    min-width: 40px;
    min-height: 40px;
    background: linear-gradient(90deg, #2979FF 0%, #4C589E 100%);
    border-radius: 6px;
    color: #fff;
    font-weight: bold;
    font-size: 32px;
    margin: 0px 4px 0px 4px;
`
export const LogoText = styled.p`
    user-select: none;  
    pointer: default; 
    font-style: normal;
    font-weight: 500;
    font-size: 36px;
    line-height: 36px;
    color: #0C2146;
`
export const SearchBar = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    max-width: 70%;
    width: 70%;
    min-width: 300px;
    max-height: 50px;
    background: #eaeaff;
    border-radius: 100px;
    box-shadow: 0px 0px 1px #fefefe;
`
export const SearchIcon = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    min-width: 30px;
    min-height: 40px;
    cursor: pointer;
`
export const SearchInput = styled.input`
    background: transparent;
    border-width: 0;
    flex: 1;
    margin: 0;
    font-size: 18px;
    padding: 5px; 
    &:focus {
        outline: none;
    }
`
export const WatchBtn = styled.div`
    display: flex;
    align-items: flex-end;
    justify-content: flex-start;
    min-height: 50px;
    min-width: 90px;
    position: relative;
    &:hover p:last-child {
        color: #fff;
        background: #9DC2FF;
    }
`
export const WatchBtnCircle = styled.p`
    cursor: pointer;
    user-select: none;  
    font-size: 12px;
    color: #fff;
    padding: 0px 5px 2px 5px;
    background: #F44336;
    border-radius: 20px;
    position: absolute;
    top: 5px;
    right: 5px;
`
export const WatchBtnText = styled.p<{fontSize?: number, bold?: boolean, open?: boolean}>`
    cursor: pointer;
    user-select: none;  
    font-size: ${p => p.fontSize ? p.fontSize : '18'}px;
    border: 1px solid #9DC2FF;
    border-radius: 4px;
    padding: 6px 10px 6px 10px;
    color: #2264D1;
    font-weight: ${p => p.bold ? 'bold' : '400'};
    line-height: 143%; 
    align-items: center; 
    letter-spacing: 0.018em;
    transition: all 0.5s ease;
    ${p => p.open && 'background: #2264D1;color: #fff;'} 
`
export const UserImgView = styled.div`
    display: flex;
    align-items: center;
    justify-content: center; 
`
export const UserFilterView = styled.div`
    display: none;
    align-items: center;
    justify-content: center; 
    font-size: 30px;
    min-width: 50px;
    @media (max-width: 750px) {
        display: flex;
    }  
`
export const UserImgCircle = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 40px;
    height: 40px;
    border-radius: 20px;
    overflow: hidden;
`
export const UserImg = styled.img`
    object-fit: cover;
    max-width: 40px;
    max-height: 40px;
`
export const TopFilterBarConatiner = styled.section`
    top: 0;
    position: sticky;
    z-index: 11;
    min-width: 100%;
    background: #fff;
    min-height: 130px;
    display: flex;
    align-items: flex-end;
    justify-content: center;
    @media (max-width: 1000px) {
        flex-direction: row;
        max-height: unset;
        min-height: unset;
        max-width: 100vw;
    } 
    @media (max-width: 750px) {
        position: relative; 
    }
`
export const TopFilterBarView = styled.div`
    min-width: 100%;
    min-height: 50px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    @media (max-width: 750px) {
        flex-direction: column;
        max-width: 100vw;
        overflow-x: auto;
        justify-content: flex-start; 
        display: -webkit-box;
    }
`
export const TopFilterBarChild = styled.div<{flexEnd?: boolean}>`
    flex-grow:1; 
    user-select: none;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: ${p => p.flexEnd ? 'flex-end' : 'flex-start'};
    @media (max-width: 750px) {
        min-height: 50px; 
    }
`
export const GridViewSwitch = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    justify-content: center;
    min-height: 50px;
    @media (max-width: 750px) {
        display: none;
    }
`
export const GridSwitchChild= styled.div<{fontSize: number, selected: boolean}>`
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    min-width: 70px;
    transition: all 0.5s ease;
    box-shadow: ${p => p.selected ? '0px 1px 2px rgba(27, 78, 163, 0.24), 0px 2px 4px rgba(41, 121, 255, 0.24)' : '0px'};
    border-radius: 4px;
    color: ${p => p.selected ? '#2979FF' : '#787885'};
    font-size: ${p => p.fontSize}px;
    padding-bottom: 5px;
    margin: 0px 2px 0px 2px;
`
export const DropDownFilterBar = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    padding: 0px 10px 0px 10px;
    position: relative;
`
export const DropDownFilterBarChildView = styled.div`
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    border-radius: 4px;
    border: 1px solid #B4B4BB;
    min-width: 136px;
    color: #787885;
    position: relative;
`
export const DropDownFilterTopLabel = styled.p`
    position: absolute;
    background: #fff;
    padding: 0px 2px 0px 2px;
    font-weight: 500;
    font-size: 10px;
    line-height: 16px; 
    align-items: center;
    letter-spacing: 1.5px;
    text-transform: uppercase; 
    color: #787885;
    z-index: 10;
    top: -10px;
`
export const DropDownFilterBarChildSelect = styled.select`
    cursor: pointer;
    padding: 10px 10px 10px 10px;
    border-width: 0;
    min-width: 100%; 
    font-weight: 500;
    font-size: 14px;
    line-height: 143%;
    align-items: center;
    letter-spacing: 0.018em;  
    color: #19191D;
    background: #fff;
`
export const ItemsTopGridBarConatiner = styled.section`
    min-width: 100%;
    background: #fff;
    min-height: 70px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    max-width: 100vw;
    overflow-x: auto;
    user-select: none; 
    z-index: 10; 
`
export const ItemsTopGridBarView = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`
export const ItemsTopGridBarChild = styled.div`
    margin: 0px 5px 0px 5px;
`
export const ItemsTopGridBarText = styled.p<{selected?: boolean}>`
    font-weight: ${p => p.selected ? 'bold' : '400'};
    text-align: center;
    font-size: 16px;
    padding: 5px;
    cursor: pointer;
    color: #4A4B57; 
    letter-spacing: 0.018em;
`
export const LeftFilterSectionConatiner = styled.section`
    background: #fff;
    min-width: 250px;
    min-height: 500px;
    display: flex;
    flex-direction: column;
    user-select: none;
    align-items: center;
    justify-content: flex-end;
    @media (max-width: 1000px) {
        flex-direction: row;
        justify-content: space-between;
        min-height: 130px;
        min-width: 100%;
        max-width: 100%;
    }
    @media (max-width: 750px) {
        flex-direction: column;
        justify-content: center;
    }
`
export const LeftFilterSectionChild = styled.div<{height: number}>`
    min-width: 100%;
    min-height: ${p => p.height}px;
    display: flex;
    flex-direction: column;
    align-items: ${p => p.height === 100 ? 'center' : 'flex-start'};
    @media (max-width: 1000px) {
        min-height: unset;
        min-width: unset; 
    }
`
export const LeftFilterText = styled.p<{bold?: boolean}>` 
    flex: 1;
    font-weight: ${p => p.bold ? 'bold' : '400'};
    font-size: 14px; 
    transition: all 0.5s ease;
    @media (max-width: 750px) {
        padding-bottom: ${p => p.bold ?'25px' : '0px'};
    }
`
export const LeftFilterIcon = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`
export const LeftFilterCustReviewGrid = styled.div`
    min-width: 100%;
    align-items: center;
    justify-content: center;
`
export const LeftFilterCustReviewGridChild = styled.div<{center?: boolean}>`
    display: flex;
    min-width: 100%;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
`
export const CustReviewChild = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    font-size: 10px;
    margin: 0px 10px 0px 10px;
    cursor: pointer;
    user-select: none;
    &: hover p {
        color: red;
    }  
`
export const ItemsGridViewConatiner = styled.section<{display: string}>`
    display: ${p => p.display};
    grid-template-columns: 1fr 1fr 1fr 1fr;
    gap: 16px;
    align-items: center;
    min-width: 100%;
    ${p => p.display === 'flex' && 'flex-direction: column;'}
    @media (max-width: 1300px) {
        grid-template-columns: 1fr 1fr 1fr;
    }
    @media (max-width: 1000px) {
        grid-template-columns: 1fr 1fr;
        max-height: unset; 
    } 
    @media (max-width: 750px) {
        grid-template-columns: 1fr;
        padding: 0px 10px 0px 10px;
    } 
`
export const ItemGridView = styled.div<{flexView?: boolean}>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    min-width: 100%;
    @media (max-width: 750px) {
        ${p => p.flexView ? 'align-items: flex-start;padding: 0px 5px 0px 5px;' : ''}
        min-width: unset;
    }
`
export const ItemGridViewRow = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start; 
    @media (max-width: 750px) {
        flex-direction: row;
    } 
`
export const ImgView = styled.img` 
    object-fit: stretch;
    max-height: 224px;
    max-width: 226px;
    min-height: 224px;
    min-width: 226px;
    cursor: pointer;
    user-select: none;  
    @media (max-width: 750px) {
        max-height: 124px;
        max-width: 126px;
        min-height: 124px;
        min-width: 126px;
    }
`
export const DescText = styled.p` 
    padding: 10px 0px 10px 0px;
    letter-spacing: 0.0275em;
    color: #19191D;
    overflow-wrap: break-word;
`
export const ItemPrice = styled.h1` 
    min-width: 100%;
    text-align: left;
    @media (max-width: 750px) {
        min-width: unset;
    } 
`
export const AdditionalInfo = styled.p` 
    padding: 10px 0px 10px 0px;
    color: #787885;
    letter-spacing: 0.018em;
    overflow-wrap: break-word;
`
export const ItemBottomBar = styled.div` 
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    min-width: 100%;
`
export const RateText = styled.p` 
    user-select: none;  
`
export const WatchItemBtn = styled.div`
    user-select: none; 
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 40px;
    min-width: 80px; 
    &:hover p:last-child {
        color: #fff;
        background: #2264D1;
    }
`