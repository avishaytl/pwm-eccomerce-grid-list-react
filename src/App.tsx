import './App.css'
import Main from './pages'
import { StoreProvider } from './stores'

function App() {
  return (
    <StoreProvider>
      <div className={`App`}> 
        <Main/>
      </div> 
    </StoreProvider>
  );
}

export default App;
