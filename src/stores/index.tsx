import React from 'react'
import { _mainStore, MainStoreType } from './Main'
import { useLocalStore } from 'mobx-react'

const StoreContext = React.createContext<{mainStore: MainStoreType | null}>({mainStore: {} as MainStoreType})

export const StoreProvider = ({ children }: any) => {
  const store = useLocalStore(_mainStore)
  return <StoreContext.Provider value={{mainStore: store}}>{children}</StoreContext.Provider>
}

export const useStore = () => {
  const store = React.useContext(StoreContext)
  if (!store) { 
    throw new Error('useStore must be used within a StoreProvider.')
  }
  return store
}