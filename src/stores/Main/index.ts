import { runInAction } from "mobx"
import { ItemProps, SliderValue, WatchList } from "../../interfaces"
import mainServices from "../../services"

interface MainStoreProps {
    items: ItemProps[]
    searchItems:  ItemProps[]
    lastUserSearchHistory: string[]
    currentUserSearchValue: string
    gridViewMode: string
    watchList: WatchList
    watchListMode: boolean
    sliderValue: SliderValue
    showModileFilter: boolean
    setSliderValue({max, min}: SliderValue): void
    setGridData(reset?:boolean, value?: string, rateSearch?: boolean, sliderSearch?: {min:number, max: number}): void
    setCurrentUserSearchValue(val: string): void
    setUserSearch(val: string): void
    setFilter(val: string):void
    setGridViewMode(mode: string): void
    setGridWatchClick(remove: boolean, index: number,item: ItemProps): void
    setUserWatchList(): void
    setWatchListFromStorage(items: ItemProps[]): void
    setWatchListMode(open?: boolean): void
    actionMobileFilter():void
    fetchGridData(): void
}

export function _mainStore(): MainStoreProps{  
    return {
      items: [{id:'1',
              price: '$0.50', 
              img: 'https://i.pinimg.com/originals/54/b6/46/54b646f8059819125109773a77798031.jpg', 
              info:'Eligible for Shipping To Mars or somewhere else', 
              rate: '⭐⭐⭐', 
              des: 'Vintage Typewriter to post awesome stories about UI design and webdev.'},
              {id:'2',
              price: '$35.00', 
              img: 'https://stylesatlife.com/wp-content/uploads/2017/05/Black-and-white-Classic-Bow-Saddle-Shoes.jpg.webp', 
              info:'Eligible for Shipping To Mars or somewhere else', 
              rate: '⭐⭐⭐⭐', 
              des: 'Vintage Typewriter to post awesome stories about UI design and webdev.'},
              {id:'3',
              price: '$23.20', 
              img: 'https://www.collinsdictionary.com/images/full/cat_156310937.jpg', 
              info:'Eligible for Shipping To Mars or somewhere else', 
              rate: '⭐⭐', 
              des: 'Vintage Typewriter to post awesome stories about UI design and webdev.'},
              {id:'4',
              price: '$129.23', 
              img: 'https://cdnprod.mafretailproxy.com/sys-master-root/h56/hf1/17485680607262/1221491_main.jpg_480Wx480H', 
              info:'Eligible for Shipping To Mars or somewhere else', 
              rate: '⭐⭐⭐⭐', 
              des: 'Vintage Typewriter to post awesome stories about UI design and webdev.'},
              {id:'5',
              price: '$49.50', 
              img: 'https://ae01.alicdn.com/kf/H0162f7ba9e49454398e4b832a9833a87a/Colorful-Artificial-Plants-Bonsai-Small-Tree-Pot-Plants-Fake-Tree-for-Home-Garden-Decoration-04.jpg_Q90.jpg_.webp', 
              info:'Eligible for Shipping To Mars or somewhere else', 
              rate: '⭐', 
              des: 'Vintage Typewriter to post awesome stories about UI design and webdev.'},
              {id:'6',
              price: '$49.50', 
              img: 'https://api.time.com/wp-content/uploads/2016/03/wellbeing-at-work.jpg', 
              info:'Eligible for Shipping To Mars or somewhere else', 
              rate: '⭐', 
              des: 'Vintage Typewriter to post awesome stories about UI design and webdev.'},
              {id:'7',
              price: '$49.50', 
              img: 'https://m.media-amazon.com/images/I/61tJPEy1OdL._AC_SL1500_.jpg', 
              info:'Eligible for Shipping To Mars or somewhere else', 
              rate: '⭐', 
              des: 'Vintage Typewriter to post awesome stories about UI design and webdev.'},
              {id:'8',
              price: '$49.50', 
              img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1cz3hpaNUJMr9hORAhd7vShHvfvBpinczvQ&usqp=CAU', 
              info:'Eligible for Shipping To Mars or somewhere else', 
              rate: '⭐', 
              des: 'Vintage Typewriter to post awesome stories about UI design and webdev.'},
              {id:'9',
              price: '$49.50', 
              img: 'https://m.media-amazon.com/images/I/71VIgiS2T-L._SY450_.jpg', 
              info:'Eligible for Shipping To Mars or somewhere else', 
              rate: '⭐', 
              des: 'Vintage Typewriter to post awesome stories about UI design and webdev.'},
            ],
      searchItems:[],
      watchList: {},
      sliderValue: {min: 0, max: 200},
      watchListMode: false,
      showModileFilter: false,
      gridViewMode: 'grid', 
      lastUserSearchHistory: ['Related'],
      currentUserSearchValue: 'Related',
      setSliderValue({max, min}){
        runInAction(()=>{
          this.sliderValue = {max, min}
        })
      },
      setWatchListFromStorage(items){
        let list = {}
        for(let i = 0;i < items.length;i++)
          list[`${i}`] = items[i]
        runInAction(()=>{
          console.log(typeof items,items)
          this.watchList = list
        }) 
      },
      setWatchListMode(open){
        runInAction(()=>{
          this.watchListMode = open !== undefined ? open : !this.watchListMode
        }) 
      },
      setGridViewMode(mode){
        runInAction(()=>{
          this.gridViewMode = mode
        })
      },
      setCurrentUserSearchValue(val){
          runInAction(()=>{
            this.currentUserSearchValue = val 
          })
      },  
      setUserSearch(val){
        runInAction(()=>{
          if(val.length && !this.lastUserSearchHistory.includes(val)){
            this.lastUserSearchHistory.push(val)
            this.setCurrentUserSearchValue(val)
            this.setGridData()
          }
        })
      }, 
      setGridData(reset, value, rateSearch, sliderSearch){ 
        if(reset){
          if(!this.searchItems.length)
            return
          runInAction(()=>{ 
              this.items = this.searchItems 
              this.searchItems = []
          }) 
          return
        }
        const items = this.searchItems.length ? this.searchItems : this.items
        const searchValue = value ? value : this.currentUserSearchValue
        let res: ItemProps[] = [];
        if(searchValue || sliderSearch){
          items.find((item) => { 
            if(sliderSearch){
              let strPrice = item.price.split('.')[0] ? item.price.split('.')[0] : item.price
              let price = typeof parseInt(strPrice.substring(1,strPrice.length)) === 'number' ? parseInt(strPrice.substring(1,strPrice.length)) : null
              if(typeof price === 'number' && price <= sliderSearch.max && price >= sliderSearch.min)
                res.push(item) 
            }else if((rateSearch && item.rate.length >= parseInt((searchValue))) || 
                ((!rateSearch && item.des.indexOf(searchValue) !== -1) ||
                  (!rateSearch && item.info.indexOf(searchValue) !== -1) ||
                  (!rateSearch && item.price.indexOf(searchValue) !== -1) || 
                  (!rateSearch && item.rate.length === parseInt((searchValue)))))
               res.push(item) 
          })
        }
        runInAction(()=>{
            if(sliderSearch)
              this.sliderValue = sliderSearch
            this.searchItems = this.searchItems.length ? this.searchItems : items
            this.items = res ? res : [] 
        }) 
      }, 
      setFilter(val){  
        let items = this.searchItems.length ? this.searchItems : this.items
        let res: ItemProps[] = []
        this.setWatchListMode(false)
        for(let i = 0;i < items.length;i++)
          res.push(items[i]) 
          res = mainServices.sortByType(val, res)

        runInAction(()=>{ 
            this.searchItems = this.searchItems.length ? this.searchItems : this.items
            this.items = !res.length ? this.searchItems : res
            if(!res.length)
              this.searchItems = []
        }) 
      },
      setGridWatchClick(remove, index, item){
          console.log('fav',item['fav'],remove,Object.keys(this.watchList))
          if((item['fav'] || remove) && this.watchList[item.id]){
            runInAction(()=>{ 
              delete this.watchList[item.id]
              this.items[index]['fav'] = undefined
            })
            if(!Object.keys(this.watchList).length && this.watchListMode){
              this.setWatchListMode() 
              runInAction(()=>{
                this.items = this.searchItems
                this.items[index]['fav'] = undefined
                this.searchItems = []
              }) 
            }else if(this.watchListMode) { 
              let items: ItemProps[] = []
              for(let id in this.watchList)
                items.push(this.watchList[id])
              runInAction(()=>{
                this.searchItems = this.searchItems.length ? this.searchItems : this.items
                this.items = items.length ? items : this.searchItems
                this.items[index]['fav'] = undefined
              }) 
            }
          }else{
            runInAction(()=>{
              item['fav'] = true
              this.items[index]['fav'] = true
              this.watchList[`${item.id}`] = item 
              mainServices.saveToLS('watchList',JSON.stringify(Object.values(this.watchList)))
            }) 
          }
      },
      setUserWatchList(){
        if(!Object.keys(this.watchList).length)
          return
        if(this.watchListMode){
          this.setWatchListMode()
          runInAction(()=>{ 
            this.items = this.searchItems
            this.searchItems = []
          }) 
        }else{
          this.setWatchListMode()
          let items: ItemProps[] = []
          for(let id in this.watchList)
            items.push(this.watchList[id])
          runInAction(()=>{
            this.searchItems = this.searchItems.length ? this.searchItems : this.items
            this.items = items.length ? items : this.searchItems
          }) 
        }
      },
      actionMobileFilter(){
        runInAction(()=>{
          this.showModileFilter = !this.showModileFilter
        })
      },
      async fetchGridData(){
        try{
          const response = await mainServices.fetchGridItemsData()
          if(response.code){ 
            let items = []
            for(let item in response.response)
              items.push(response.response[item])
            if(items.length)
              runInAction(()=>{
                this.items = items
              })
          }
        }catch({message}){
          console.log('fetchGridData',message)
        }
      }
    }
}
  
export type MainStoreType = ReturnType<typeof _mainStore>