interface MainServicesProps {
    sortByType: (type: string, res) => []
    sortByPrice: ({type: string, a, b}) => number
    getFromLS: (key: string) => string
    removeFromLS: () => void
    saveToLS: (key: string, value: string) => void
    fetchGridItemsData: () => Promise<{code: number,response: []}>
}

const _mainServices = (): MainServicesProps => {
    return({
        async fetchGridItemsData(){
            try{ 
                const response = await fetch("http://localhost:4000/getListData", {method: 'GET'}) 
                if(response && response.ok)
                    return await response.json()

            }catch({message}){
                console.log('fetchGridItemsData',message)
                return {code: 0, response: message}
            }
        },
        sortByType(type, res){ 
            switch(type){
                case 'Useless first': 
                    return [] 
                case 'Cheapest': 
                res.sort((a, b)=>{ 
                    return this.sortByPrice({type: 'low',a,b})
                })
                break;
                case 'Most Expensive': 
                res.sort((a, b)=>{
                    return this.sortByPrice({type: 'high',a,b})
                })
                break;
                case 'Top Rate': 
                res.sort((a, b)=>{
                    return  b['rate'].length - a['rate'].length
                })
                break;
                default:{ 

                }
            } 
            return res
        },
        sortByPrice: ({type, a, b}) =>{
            let strPricea = a.price.split('.')[0] ? a.price.split('.')[0] : a.price
            let pricea = typeof parseInt(strPricea.substring(1,strPricea.length)) === 'number' ? parseInt(strPricea.substring(1,strPricea.length)) : null
            let strPriceb = b.price.split('.')[0] ? b.price.split('.')[0] : b.price
            let priceb = typeof parseInt(strPriceb.substring(1,strPriceb.length)) === 'number' ? parseInt(strPriceb.substring(1,strPriceb.length)) : null
            if(typeof pricea !== 'number')
              return 0
            if(typeof priceb !== 'number')
              return 0
            return type === 'low' ? pricea - priceb : priceb - pricea
        }, 
        getFromLS(key){
            let ls:any = {};
            if (global.localStorage){
                try {
                    ls = JSON.parse(global.localStorage.getItem("rgl-7") as string) || {};
                } catch ({message}) {
                    alert(message)
                }
            }
            return ls[key];
        },
        removeFromLS(){ 
            if (global.localStorage){
                try {
                    global.localStorage.removeItem("rgl-7") 
                } catch ({message}) { 
                    alert(message)
                }
            } 
        },
        saveToLS(key, value){
            if (global.localStorage){
                global.localStorage.setItem(
                    "rgl-7",
                    JSON.stringify({[key]: value})
                );
            }
        }
    })
}

export default _mainServices